<?php
    //Yay hex2int
    function hex2int($hex){
        //Set total to 0
        $total = 0;
        //loop through the strings length
        for($i = 0; $i<strlen($hex); $i++){
            //Find the power modifier
            $pow = (strlen($hex)-1) - $i;
            //Check if the char is a number
            if(is_numeric($hex[$i])){
                //Do the conversion
                $total += intval($hex[$i]) * pow(16, $pow);
            }
            else{
                //If the number is a letter use the intval function to convert it to hex
                $total += intval($hex[$i], 16) * pow(16, $pow);
            }
        }
        return $total;
    }
    
    //Not using hexdec() but still using built in function - loophole?
    function hex2intlite($hex){
        //return the intval conversion of the hex string
        return intval($hex, 16);
    }
    
    //ECHO ᴇᴄʜᴏ ᵉᶜʰᵒ ₑ𝒸ₕₒ 
    echo "<h4>Hex2Int Equation (#BFF): </h4>";
    echo hex2int('BFF');
    echo "<br>";
    echo "<h4>Technically cheating but still follows homework spec (#FFF): </h4>";
    echo hex2intlite('FFF');
?>