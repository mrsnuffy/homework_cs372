<?php
    function connect($username, $password, $database)
    {
        if(empty($username) || empty($password) || empty($database){
			die('All parameters required');
		}
		    
		$connection = new mysqli('localhost', $username, $password, $database);
		if ($connection->connect_error) {
            die('Error (' . $connection->connect_errno . ') '. $connection->connect_error);
    	}
    	
    	return $connection;
    }
?>