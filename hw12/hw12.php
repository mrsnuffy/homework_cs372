<?php 
	require('../helpers/databaseConnectionHelper.php');

    $refers = array(
    		"Google",
			"Friend",
			"Advert",
			"Other"
    	);

    if (isset($_POST["review"]))
    {
        if (empty($_POST["name"]) || empty($_POST["email"]) || empty($_POST["refer"]) || empty($_POST["rating"]) || empty($_POST["comment"]) || empty($_POST["subscribe"]))	
        {
			$errorText = 'Fill out the form.'
	    }
	    else
    	{
    		$connection = connect('test', 'test', 'test');
    		$name = $_POST["name"];
		    $email = $_POST["email"];
		    $ref = $_POST["refer"];
		    $rating = $_POST["rating"];
		    $comment = $_POST["comment"];
    		$sql = sprintf("INSERT INTO REVIEWS ('NAME', 'EMAIL', 'REFER', 'RATING', 'COMMENT', 'SUBSCRIBE') 
    			VALUES ('$name', '$email', '$ref', '$rating', '$comment', '$subscribe' );");
    		
    		$result = $connection->query($sql) or die(mysqli_error($connection));
    		header("Location: submit.php");
    		exit;
    	}
    }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Submit your Review!</title>
	</head>
	<body>
		<?php if (isset($errorText)): ?>
        		<div style="color: red"><?php echo $errorText ?></div>
      	<?php endif ?>
		<form action="<?php $_SERVER["PHP_SELF"] ?>" method="post">
			<fieldset>
				<legend>Your Details:</legend>
				<label>Name: <input type="text" name="name" value = "<?php if (isset($_POST["name"])){ echo $_POST["name"];}?>"></label>
				<br/>
				<label>Email: <input type="email" name="email" value = "<?php if (isset($_POST["email"])){ echo $_POST["email"];}?>"></label>
				<br/>
			</fieldset>
			<br/>
			<fieldset>
				<legend>Your Review:</legend>
				<p>
					<label for="refer">How did you hear about us?</label>
					<select name="refer" id="refer">
						<?php
							foreach($refers as $refer)
							{
								if (isset($_POST["refer"]) && $_POST["refer"] == $refer)
									echo "<option selected = 'selected' value='$refer'>$refer</option>";
								else
									echo "<option value='$refer'>$refer</option>";
							}
						?>
					</select>
				</p>
				<p>
					Would you visit again?<br />
					<label><input type="radio" name="rating" value="yes" <?php if (isset($_POST["rating"]) && $_POST["rating"] == "yes"){ echo "checked"};?>/> Yes</label>
					<label><input type="radio" name="rating" value="no" <?php if (isset($_POST["rating"]) && $_POST["rating"] == "no"){ echo "checked"; }?>/> No</label>
					<label><input type="radio" name="rating" value="maybe" <?php if (isset($_POST["rating"]) && $_POST["rating"] == "maybe"){echo "checked"};?>/> Maybe</label>
				</p>
				<p>
					<label for="comment">Comments:</label><br />
					<textarea id="comment" name="comment"><?php if (isset($_POST["comment"])){ echo $_POST["comment"]; }else{ echo "";}?></textarea>
				</p>
				<label><input type="checkbox" name="subscribe" 
						<?php if (!isset($_POST["review"]) || (isset($_POST["subscribe"]) && $_POST["subscribe"] == "on")) {echo "checked";}?> /> Sign me up for email updates
				</label>
				<br/>
				<input type="submit" name="review" value="Submit review" />
			</fieldset>
		</form>
	</body>
</html>