<?php
    require('../helpers/databaseConnectionHelper.php');
    require('../helpers/tableHelper.php');
    

    $connection = connect('test', 'test', 'test');

    if ($connection->connect_error) {
        die('Connect Error (' . $connection->connect_errno . ') '. $connection->connect_error);
    }
    
    $sql = sprintf("CREATE TABLE IF NOT EXISTS COURSES(SEMESTER VARCHAR(30) NOT NULL ,COURSE VARCHAR(30) NOT NULL);");
    
    $result = $connection->query($sql) or die(mysqli_error());   
    
    $courses = array(
        "Spring 2019" => "Human Computer Interactions", 
        "Spring 2019" => "Senior Capstone Part 2"
    );
    
    foreach($courses as $semester => $course)
    {
        $sql = "INSERT INTO COURSES VALUES('" . $semester . "', '" . $course . "');";
        $result = $connection->query($sql) or die(mysqli_error());
    }
    
    echo "<html>\n";
    createNewTable("COURSES", $connection, "[database]");
    echo "</html>\n";
?>