function Validate(){
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var email = document.getElementById("email").value;
    var select = document.getElementById("select").value;
    var comment = document.getElementById("comment").value;

    if(!firstName){
        alert("Enter your firstname");
    }
    else if(!lastName){
        alert("Enter your lastname");
    }
    else if(!email.match(/.+@.+\.edu$/)){
        alert("Enter a valid .edu address");
    }
    else if(!select){
        alert("Please select how you heard about us");
    }
    else if(!comment){
        alert("Enter a comment");
    }
}

function UpdateButton(){
    var emailCheck = document.getElementById("emailCheck").checked;
    if(emailCheck == true){
        
        document.getElementById("subButton").disabled = false;
    }
    else{
        document.getElementById("subButton").disabled = true;
    }
}