<!DOCTYPE html>
<html>
    <head>
        <title>Calculator</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="index.js"></script>
    </head>
    <body style="margin:20px;">
        <h1>Calculator!</h1>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Number 1</span>
            </div>
            <input type="number" class="form-control" placeholder="Number 1" id="num1">
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Number 2</span>
            </div>
            <input type="number" class="form-control" placeholder="Number 2" id="num2">
        </div>
        <div class="row">
            <button type="button" class="btn btn-primary ml-3" onclick="Add()">Add</button>
            <button type="button" class="btn btn-primary ml-1" onclick="Sub()">Sub</button>
            <button type="button" class="btn btn-primary ml-1" onclick="Mul()">Mul</button>
            <button type="button" class="btn btn-primary ml-1" onclick="Div()">Div</button>
        </div>
        <div class="input-group mb-3 mt-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Result</span>
            </div>
            <input type="number" class="form-control" placeholder="" id="result">
        </div>
    </body>
</html>