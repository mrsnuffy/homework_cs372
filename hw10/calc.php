<?php
$num1 = floatval($_POST['num1']);
$num2 = floatval($_POST['num2']);

if(!isset($num1)){
    die("Input first number");
}
if(!isset($num2)){
    die("Input second number");
}
switch($_POST['type']){
    case 'add':
        Add($num1, $num2);
        break;
    case 'sub':
        Sub($num1, $num2);
        break;
    case 'mul':
        Mul($num1, $num2);
        break;
    case 'div':
        Div($num1, $num2);
        break;
}
function Add($num1, $num2){
    $result = $num1+$num2;
    echo $result;
}

function Sub($num1, $num2){
    $result = $num1-$num2;
    echo $result;
}

function Div($num1, $num2){
    $result = $num1/$num2;
    echo $result;
}

function Mul($num1, $num2){
    $result = $num1*$num2;
    echo $result;
}
?>