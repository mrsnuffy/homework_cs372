<?php
    $options = ["Google", "Friend", "Advert", "Other"];
    
      if(!isset($_POST['checkbox'], $_POST['firstName'], $_POST['comments'], $_POST['radios'], $_POST['options'], $_POST['email'], $_POST['lastName'])){
        $checkChecked = "checked";
        $firstName = "Jared";
        $lastName = "Lehman";
        $email = "lehmja02@pfw.edu";
        $optionVal = "Advert";
        $comments = "I LIKE CS372! IT'S PRETTY NEAT!";
        echo "<h4 style='color:red;'>Please input all form elements</h4>";
      }
      else{
        echo "<h1>Thanks for giving us all your info. Mwahahaha! <h1>";
        return;
      }
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <title></title>
      </head>
    <body class="col">
      <form class="col mb-3 mt-3" style="border: 1px solid lightgrey; border-radius:15px;" action="hw3.php" method="POST" name="form">
        <legend>Your Details:</legend>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">First and last name</span>
          </div>
          <input type="text" name="firstName" placeholder="First Name" aria-label="First name" class="form-control" value="<?php echo $firstName ?>">
          <input type="text" name="lastName" placeholder="Last Name" aria-label="Last name" class="form-control" value="<?php echo $lastName ?>">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-default">Email</span>
          </div>
          <input placeholder="Email" name="email" type="email" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $email ?>">
        </div>
        <legend>Your Review:</legend>  
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-default">How did you hear about us?</span>
          </div>
          <select class="form-control" name="options" value="<?php echo $optionVal ?>">
          <?php 
            foreach($options as $opt){ ?>
                <option value="<?php echo $opt?>"><?=$opt?></option>
            <?php } ?>
          </select>
        </div>
        <h6>Would you visit us again?</h6>
        <div class="row m-3">
          <div class="form-check mr-2">
            <input class="form-check-input" type="radio" name="radios" id="exampleRadios1" value="option1" checked>
            <label class="form-check-label" for="exampleRadios1">
              Yes
            </label>
          </div>
          <div class="form-check  mr-2">
            <input class="form-check-input" type="radio" name="radios" id="exampleRadios2" value="option2">
            <label class="form-check-label" for="exampleRadios2">
              No
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="radios" id="exampleRadios3" value="option3">
            <label class="form-check-label" for="exampleRadios3">
              Maybe
            </label>
          </div>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-default">Comments</span>
          </div>
          <textarea type="textarea" name="comments" class="form-control" placeholder="Additional Comments" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"><?php echo $comments ?></textarea>
        </div>
        <div class="form-group form-check">
          <input type="checkbox" name="checkbox" class="form-check-input" id="exampleCheck1" <?php echo $checkChecked ?>>
          <label class="form-check-label" for="exampleCheck1">Sign me up for email updates!</label>
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit Review</button>
      </div>
    </body>
</html>
