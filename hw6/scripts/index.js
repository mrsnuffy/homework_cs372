var divNames = ["resume", "video", "courses", "location", "favoriteLinks"];

function navigate(showDivName){
    for(var i=0; i<divNames.length; i++){
        document.getElementById(divNames[i]).style.display = "none";
    }
    document.getElementById(showDivName).style.display = "block";
}
