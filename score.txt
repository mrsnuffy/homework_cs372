Jared CS372 Score report

Homework1 - 100/100
Homework2 - 100/100
Homework3 - 100/100
Homework4 - 100/100
Homework5 - 100/100
Homework6 - 96/100
Homework7 - 100/100
Homework8 - 100/100
Homework9 - 100/100
Homework10 - 85/100
Homework11 - 85/100
Homework12 - 60/100


Note:
HW6: (-4 pts) The table should distinguish even rows from odd rows by the background color. When the mouse is over a row, it should show a different color. 

HW10 : (-10 pts) Review page reloading with default values after submitting an empty form.
       (-5 pts) Able to submit the form even if the Name fields, Email field or Comments textarea is empty.

HW11: Q1(-5 pts) Should get avg of all the courses. 
      Q2(-10 pts) The query submitted doesn't compute the average evaluation score.

select professorid, avg(evaluation) avgprof from CourseEvaluation group by professorid
having avgprof > (select avg(profeva) aveeva from (select professorid, avg(evaluation) profeva
from courseevaluation group by professorid));

HW12: (-20 pts) Late submission penalty
      (-20 pts) Q1 and Q2 -> Many compilation errors in the php code.
      Use DRY principle -> $connection = connect('test', 'test', 'test');
      Instead of adding this line to every php file, add the common code to databaseConnectionHelper.php 