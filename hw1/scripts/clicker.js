/*global $*/
/* Written by Jared Lehman circa August 2018 */

$( document ).ready(function() {
    $("#alertText").hide();
});

var clickCount = 0;

var clickArr = [
        "Stop clicking.",
        "For real stop clicking.",
        "If you keep clicking then something bad will happen.",
        "Alright, you think your funny.",
        "hahaha",
        "HAHAHAHAHA",
        "One more time.",
        "That's it."
    ];


function beginClicker(){
    $("#alertText").show();
    if(clickCount < clickArr.length){
        $("#alertText").text(clickArr[clickCount]);
        clickCount++;
    }
    else{
        $("#alertText").hide();
        goodbyeWorld();
    }
}

function goodbyeWorld(){
    $("#navText").text("Goodbye World.");
    $("#navbarId").removeClass("bg-dark");
    $('#navbarId').addClass("bg-danger");
    $("#header").text("WORLD ENDING IN: 5");
    $("#hideContent").hide();
    CountDown(5);
}

function CountDown(counter){
    var number = counter-1;
    if(number>=0){
        setTimeout(function(){
            $("#header").text("WORLD ENDING IN: "+number);
            CountDown(number);
        }, 1000);
    }
    else{
        $("#header").text("Goodbye World.");
        setTimeout(function(){
        window.location = "https://omfgdogs.com";
        }, 1000);
    }
}
